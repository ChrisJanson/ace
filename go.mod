module ace

go 1.12

require (
	github.com/allegro/bigcache v1.2.0 // indirect
	github.com/aristanetworks/goarista v0.0.0-20190308231643-e9fb69a13f45 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/begmaroman/mpt v0.0.0-20181018140259-ba28a12396df
	github.com/btcsuite/btcd v0.0.0-20190523000118-16327141da8c
	github.com/cbergoon/merkletree v0.0.0-20181226014223-2111568b5558
	github.com/davecgh/go-spew v1.1.1
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/dgraph-io/badger v1.6.0
	github.com/ethereum/go-ethereum v1.8.22-0.20190313125352-1a29bf0ee2c5
	github.com/fd/go-nat v1.0.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/ipfs/go-log v0.0.1
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/libp2p/go-conn-security v0.0.1 // indirect
	github.com/libp2p/go-eventbus v0.1.0 // indirect
	github.com/libp2p/go-libp2p v0.3.0
	github.com/libp2p/go-libp2p-core v0.2.2
	github.com/libp2p/go-libp2p-crypto v0.1.0
	github.com/libp2p/go-libp2p-discovery v0.1.0
	github.com/libp2p/go-libp2p-host v0.1.0 // indirect
	github.com/libp2p/go-libp2p-interface-pnet v0.0.1 // indirect
	github.com/libp2p/go-libp2p-kad-dht v0.2.0
	github.com/libp2p/go-libp2p-kbucket v0.2.0
	github.com/libp2p/go-libp2p-metrics v0.0.1 // indirect
	github.com/libp2p/go-libp2p-net v0.0.1
	github.com/libp2p/go-libp2p-peer v0.2.0
	github.com/libp2p/go-libp2p-peerstore v0.1.3
	github.com/libp2p/go-libp2p-protocol v0.0.1
	github.com/libp2p/go-libp2p-pubsub v0.1.1
	github.com/libp2p/go-libp2p-routing v0.1.0
	github.com/libp2p/go-libp2p-secio v0.2.0
	github.com/libp2p/go-libp2p-transport v0.0.4 // indirect
	github.com/libp2p/go-tcp-transport v0.1.0
	github.com/libp2p/go-testutil v0.0.1 // indirect
	github.com/libp2p/go-ws-transport v0.1.0
	github.com/multiformats/go-multiaddr v0.0.4
	github.com/onrik/gomerkle v0.0.0-20170608063914-ae4e74c60b38
	github.com/rcrowley/go-metrics v0.0.0-20181016184325-3113b8401b8a // indirect
	github.com/sony/sonyflake v0.0.0-20181109022403-6d5bd6181009 // indirect
	github.com/whyrusleeping/go-logging v0.0.0-20170515211332-0457bb6b88fc
	github.com/whyrusleeping/go-smux-multiplex v3.0.16+incompatible // indirect
	github.com/whyrusleeping/go-smux-multistream v2.0.2+incompatible // indirect
	github.com/whyrusleeping/go-smux-yamux v2.0.9+incompatible // indirect
	github.com/whyrusleeping/yamux v1.1.5 // indirect
	github.com/zippoxer/bow v0.0.0-20190216135715-5ed1b7ff40c2
	go.etcd.io/bbolt v1.3.2 // indirect
	golang.org/x/crypto v0.0.0-20190618222545-ea8f1a30c443
	gopkg.in/karalabe/cookiejar.v2 v2.0.0-20150724131613-8dcd6a7f4951 // indirect
	rsc.io/quote v1.5.2
)
