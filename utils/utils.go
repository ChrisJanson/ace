package utils

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"

	logging "github.com/ipfs/go-log"
)

var log = logging.Logger("utils")

func Serialize(e interface{}) []byte {
	var res bytes.Buffer
	encoder := gob.NewEncoder(&res)

	err := encoder.Encode(e)

	if err != nil {
		log.Error(err)
	}

	return res.Bytes()
}

func Deserialize(data []byte, e interface{}) {
	decoder := gob.NewDecoder(bytes.NewReader(data))
	err := decoder.Decode(e)

	if err != nil {
		log.Error(err)
	}
}

func Hash(content []byte) string {
	h := sha256.New()
	h.Write(content)
	return hex.EncodeToString(h.Sum(nil))
}
