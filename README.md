# Project ACES Blockchain

## Project Dokumentation
All documentation lives in the folder `doc` of this repository.
```
doc/
   ansible (vm setup and deployment)/
   local environment setup/
   project documentation/
```

To build and install the project on your local development machine, the folder `local environment setup` is the place to find information like that. Start with the [Installation Instructions](https://gitlab.com/ChrisJanson/ace/blob/master/doc/local%20environment%20setup/installation.md)

The project uses VMs from Hochschul Rechenzentrum to test our blockchain environment on multiple instances. We use the Ansible deployment framework to install prerequisites, clone the project repository and update the repository on the vms.
The folder `ansible` contains information on how to install ansible and how to use it for the project. Start with [VM Deployment with Ansible](https://gitlab.com/ChrisJanson/ace/blob/master/doc/ansible%20(vm%20setup%20and%20deployment)/Ansible.md)


To get an Overview of the Project and technical Implementation documentation, visit the folder `project documentation`. You will find an architectural overview, a project overview and goals and a current state. Also the individual project parts and their interplay can be found here.