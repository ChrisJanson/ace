package main

import (
	"context"
	"fmt"
	"time"

	"bytes"
	"crypto/ecdsa"
	"encoding/hex"

	"github.com/ethereum/go-ethereum/common"
	ethtrie "github.com/ethereum/go-ethereum/trie"

	golog "github.com/ipfs/go-log"

	"ace/blockchain"
	"ace/consensus"

	"github.com/joho/godotenv"
	peer "github.com/libp2p/go-libp2p-core/peer"
	gologging "github.com/whyrusleeping/go-logging"
)

var chain *blockchain.BlockChain

func main() {
	godotenv.Load()

	//testP2P()
	testP2PSender()

	// Test functionality of the Blockchain
	//testBlockchain()
}

func testP2P() {
	golog.SetAllLoggers(gologging.DEBUG)

	// CREATE CONTEXT
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	consensus.NewNode(ctx)

	select {}
}

func testP2PSender() {
	// create testblock
	chain = blockchain.InitBlockChain()
	defer chain.Db.Close()

	privatekey := blockchain.CreatePrivateKey()

	tx := createTransaction(privatekey)
	verifyTransaction(tx, privatekey.PublicKey)

	txs := []*blockchain.Transaction{tx, tx, tx}

	testblock := chain.AddBlock("This is a Test Block with 3 Transaction", txs)
	fmt.Println(testblock)

	golog.SetAllLoggers(gologging.DEBUG) // Change to DEBUG for extra info

	// CREATE CONTEXT
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	node := consensus.NewNode(ctx)

	var Testpeer peer.ID

	for Testpeer == "" {
		Testpeer = node.MessageHandler.Network.GetNearestPeerID()
	}

	fmt.Println("Establishing Connection to Testpeer " + Testpeer.String())

	node.SendBlock(ctx, Testpeer, *testblock)
	//node.SendRequest(ctx)

	select {}
}

func testBlockchain() {
	// Get or Create Chain from Database
	chain := blockchain.InitBlockChain()
	defer chain.Db.Close()

	privatekey := blockchain.CreatePrivateKey()

	tx := createTransaction(privatekey)
	verifyTransaction(tx, privatekey.PublicKey)

	createTestBlock(chain, tx)

	// creates Database Connection for MPT
	chain.CreateDatabaseConnection()
	createMPTExample(chain)
	testMPT(chain)

	outputEntireChain(chain)

}

func outputEntireChain(chain *blockchain.BlockChain) {
	fmt.Println("------------------------Blockchain---------------------------")
	for _, block := range chain.Blocks {
		fmt.Println("-------------------------------------------------------------")
		fmt.Printf("Previous Hash: %x\n", block.PrevHash)
		fmt.Printf("Data in Block: %s\n", block.Data)
		fmt.Printf("Hash: %x\n", block.Hash)
		fmt.Printf("BlockId: %d\n", block.BlockID)
		fmt.Println("Timestamp: ", time.Unix(block.Timestamp, 0).Format(time.RFC3339))
		fmt.Println("-------------------------------------------------------------")
	}
}

func createTransaction(privatekey *ecdsa.PrivateKey) *blockchain.Transaction {
	// Create a Transaction
	tx := blockchain.CreateNewTransaction([]byte("A"), []byte("B"), 100, privatekey)
	ID := hex.EncodeToString(tx.ID)
	fmt.Println("----------------------Create Transaction---------------------")
	fmt.Println("Transaction with created with ID: ", ID)
	return tx

}

func verifyTransaction(tx *blockchain.Transaction, publickey ecdsa.PublicKey) {
	fmt.Println("Transaction sign by Creater: ", blockchain.VerifyTx(publickey, tx))
}

func createTestBlock(chain *blockchain.BlockChain, tx *blockchain.Transaction) {

	txs := []*blockchain.Transaction{tx, tx, tx}

	chain.AddBlock("This is a Test Block with 3 Transaction", txs)

	/*
		merkleTree := blockchain.GetMerkleTreeDB(chain.Blocks[1].MerkleRoot, chain.Db)

		merkleTree1 := blockchain.CreateMerkleTree(txs)

		proof := merkleTree1.GetProof(0)
		proof = merkleTree.GetProof(0)
		leaf := merkleTree.GetLeaf(0)

		if merkleTree == merkleTree1 {
			fmt.Println("SAME")
		}

		println(merkleTree.VerifyProof(proof, merkleTree.Root(), leaf))
	*/

}

// createMPTExample - Creates a MPT with example Values and save it

func createMPTExample(chain *blockchain.BlockChain) {
	mpt := chain.NewEmptyMPT()
	mpt.Update([]byte("dog"), []byte("1"))
	mpt.Update([]byte("cat"), []byte("2"))
	mpt.Update([]byte("lion"), []byte("3"))
	mpt.Update([]byte("horse"), []byte("4"))
	mpt.Update([]byte("fish"), []byte("5"))

	root, err := mpt.Commit(nil)
	if err != nil {
		print(err)
		fmt.Println(root.Hex())
	}

}

func testMPT(chain *blockchain.BlockChain) {
	// Creates Database Connection

	trie := getExampleMPT(chain)

	fmt.Println("------------------------Test MPT-----------------------------")

	amount, err := trie.TryGet([]byte("dog"))
	if err != nil {
		print(err)
	}
	if bytes.Equal(amount, []byte("1")) {
		fmt.Println("Test Succesfull")
		fmt.Println("Balance of dog: ", string(amount))
	} else {
		fmt.Println("--- Test Failed ---")
	}

}

// getExampleMPT - get the createt ExampleTree by its Hash (Can be used with the root - trie.Hash())
func getExampleMPT(chain *blockchain.BlockChain) *ethtrie.Trie {
	trie, err := chain.GetMPT(common.HexToHash("5429dce681d76271e73a2ccff247f4b01165a38a8479034ff38e4ce1eefd982a"))
	if err != nil {
		fmt.Println(err)
	}
	return trie
}
