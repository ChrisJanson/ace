package blockchain

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"math/big"
)

type Transaction struct {
	ID     []byte
	From   []byte
	To     []byte
	Value  int64
	TxHash []byte
	Sig    Signature
}

type Signature struct {
	Sig []byte
	R   *big.Int
	S   *big.Int
}

func GenerateID() []byte {
	var encoded bytes.Buffer
	var hash [32]byte

	hash = sha256.Sum256(encoded.Bytes())
	return hash[:]
}

func createHash(id, from, to []byte, value int64) []byte {
	info := bytes.Join([][]byte{id, from, to, ToHex(value)}, []byte{})
	hash := sha256.Sum256(info)
	return hash[:]
}

func CoinbaseTx(to, data string) *Transaction {
	if data == "" {
		data = fmt.Sprintf("Coins to %s", to)
	}
	signature := Signature{nil, nil, nil}
	tx := Transaction{nil, nil, nil, 100, createHash(nil, nil, nil, 100), signature}

	return &tx
}

func CreateNewTransaction(from, to []byte, value int64, privatekey *ecdsa.PrivateKey) *Transaction {

	ID := GenerateID()
	hash := createHash(ID, from, to, value)
	tx := Transaction{ID, from, to, value, hash, SignTx(hash, privatekey)}

	return &tx
}

// checkBalance - Check in the MPT if the Account have enough balanace to do a Transaction
// if accbalance > value, return true
// else return false
func (chain *BlockChain) checkBalance(value int64, from []byte) bool {

	amount, _ := chain.MPT.TryGet(from)
	if int64(binary.BigEndian.Uint64(amount)) > value {
		return true
	} else {
		return false
	}

}

func (chain *BlockChain) ExecuteTransaction(tx *Transaction) bool {

	if chain.checkBalance(tx.Value, tx.From) == true {

		return true
	} else {
		return false
	}
}

// Sign a Transaction
func SignTx(hash []byte, privatekey *ecdsa.PrivateKey) Signature {
	return Sign(privatekey, hash)
}

func VerifyTx(pubkey ecdsa.PublicKey, tx *Transaction) bool {

	return ecdsa.Verify(&pubkey, tx.TxHash, tx.Sig.R, tx.Sig.S)
}
