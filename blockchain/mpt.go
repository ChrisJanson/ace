package blockchain

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethdb/leveldb"
	"github.com/ethereum/go-ethereum/trie"
)

// NewEmptyMPT - creates a Empty MPT
func (chain *BlockChain) NewEmptyMPT() *trie.Trie {

	chain.MPT, _ = trie.New(common.Hash{}, chain.DbMPT)

	return chain.MPT
}

// CreateDatabaseConnection - creates and save the LevelDB Connection in the chain Struct
func (chain *BlockChain) CreateDatabaseConnection() {
	diskdb, err := leveldb.New("db-mpt", 256, 0, "")
	if err != nil {
		panic(fmt.Sprintf("can´t create/open directory : %v", err))
	}
	chain.DbMPT = trie.NewDatabase(diskdb)
}

// GetMPT - returns a wanted Tree from the root Hash
func (chain *BlockChain) GetMPT(root common.Hash) (*trie.Trie, error) {
	return trie.New(root, chain.DbMPT)
}
