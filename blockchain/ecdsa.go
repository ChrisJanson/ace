package blockchain

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
)

func CreatePrivateKey() *ecdsa.PrivateKey {

	pubkeyCurve := elliptic.P256()

	privatekey := new(ecdsa.PrivateKey)
	privatekey, err := ecdsa.GenerateKey(pubkeyCurve, rand.Reader) // this generates a public & private key pair

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return privatekey
}

// Get Pubkey of a private key
func GetPublicKey(privatekey ecdsa.PrivateKey) ecdsa.PublicKey {
	var pubkey ecdsa.PublicKey
	pubkey = privatekey.PublicKey

	return pubkey
}

func Sign(privatekey *ecdsa.PrivateKey, hash []byte) Signature {
	// Sign ecdsa style

	r := big.NewInt(0)
	s := big.NewInt(0)

	r, s, serr := ecdsa.Sign(rand.Reader, privatekey, hash)
	if serr != nil {
		fmt.Println(serr)
		os.Exit(1)
	}

	signature := r.Bytes()
	signature = append(signature, s.Bytes()...)

	var Sig Signature
	Sig.Sig = signature
	Sig.R = r
	Sig.S = s
	//fmt.Printf("Signature : %x\n", signature)

	return Sig
}
