package blockchain

import (
	"crypto/ecdsa"
)

type verifyInstance struct {
	privateKey *ecdsa.PrivateKey
	publicKey  ecdsa.PublicKey
}

func createVerifyInstance() verifyInstance {
	private := CreatePrivateKey()
	public := GetPublicKey(*private)
	instance := verifyInstance{private, public}
	return instance
}

type Wallet struct {
	privateKey *ecdsa.PrivateKey
	publicKey  []byte
	sig        Signature
}

func createWallet() *Wallet {
	instance := createVerifyInstance()
	private := CreatePrivateKey()
	public := GetPublicKey(*private).Y.Bytes()
	sign := Sign(instance.privateKey, public)
	wallet := Wallet{private, public, sign}
	return &wallet
}
