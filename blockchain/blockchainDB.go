package blockchain

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"

	"github.com/onrik/gomerkle"

	"github.com/dgraph-io/badger"
)

const (
	dbPath = "db"
)

// InitDatabase Opens the Badger database on dbPath if one exist, else it creates one
// also checks if there is a Blockchain (Yes - set BlockID | No - create Genesis Block)
// Returns the badger.DB Value
func InitDatabase() *badger.DB {

	// Opens or create Database
	opts := badger.DefaultOptions("tmp/badger")
	opts.Dir = dbPath
	opts.ValueDir = dbPath
	db, err := badger.Open(opts)
	if err != nil {
		log.Fatal(err)
	}

	checkDataSet(db)

	return db
}

// checkDataSet - Create Nemesis Block if Database is Empty or Set BlockID for create new Blocks
// Database is Empty if key lastHash is not found -> Create Genesisblock
func checkDataSet(db *badger.DB) {

	err := db.Update(func(txn *badger.Txn) error {
		// check if we have a lasthash in the Database
		if _, err := txn.Get([]byte("lastHash")); err == badger.ErrKeyNotFound {

			// If there is no lasthash in the Database create the Genesisblock and write
			// its Hash to the Database in (lasthash, [])
			fmt.Println("No existing blockchain found")
			genesis := Genesis()
			fmt.Println("Genesis generated")
			err := txn.Set(SerializeID(genesis.BlockID), BlockSerialize(genesis))
			Handle(err)
			// Write the lastHash to the database
			err = txn.Set([]byte("lastHash"), genesis.Hash)
			// write BlockID to the Database
			err = txn.Set([]byte("BlockID"), SerializeID(1))
			return err

		} else { // If there is a lasthash we need the last BlockID

			fmt.Println("Blockchain exists")

			return err
		}
	})

	Handle(err)
}

/* --- TESTCASES FOR DB ---
func AddTest(db *badger.DB) {
	err := db.Update(func(txn *badger.Txn) error {

		err := txn.Set([]byte("test"), []byte("Test: Working"))
		return err
	})
	Handle(err)

}
func GetTest(db *badger.DB) []byte {

	var block []byte

	err := db.Update(func(txn *badger.Txn) error {
		if item, err := txn.Get([]byte("test")); err == badger.ErrKeyNotFound {
			fmt.Println("Test: failed")
			return err
		} else {
			block, err = item.Value()
			fmt.Println(block)
			return err
		}
	})

	Handle(err)
	return block
}
*/

func GetBlockID(db *badger.DB) int64 {

	var block_ID int64
	// Write the actual last BlockID in the Value Block_id from block.go
	err := db.View(func(txn *badger.Txn) error {

		item, err := txn.Get([]byte("BlockID"))
		var id []byte
		err = item.Value(func(val []byte) error {
			id = val
			return nil
		})
		block_ID = DeserializeID(id)
		return err

	})
	Handle(err)
	return block_ID
}

// AddBlockDB - Adds a created block to the Database
// needs the Block and the db Connection
func AddBlockDB(block *Block, db *badger.DB) {
	err := db.Update(func(txn *badger.Txn) error {

		err := txn.Set(SerializeID(block.BlockID), BlockSerialize(block))
		err = txn.Set([]byte("BlockID"), SerializeID(block.BlockID+1))
		return err
	})

	Handle(err)
}

// Add the Merkletree to the Database
func AddMerkleTreeDB(t *gomerkle.Tree, db *badger.DB) {
	err := db.Update(func(txn *badger.Txn) error {

		err := txn.Set(t.Root(), SerializeMerkleTree(t))
		return err
	})
	Handle(err)
}

// GetBlockDB - Gets a block from the Database
// needs the BlockID and the db Connection
func GetBlockDB(id int64, db *badger.DB) *Block {

	var block []byte

	err := db.View(func(txn *badger.Txn) error {
		// If errKeyNotFound  = true, Block not found
		if item, err := txn.Get(SerializeID(id)); err == badger.ErrKeyNotFound {
			fmt.Printf("No Block with this ID: %d", id)
			return err
		} else { // get the Block

			err = item.Value(func(val []byte) error {
				block = val
				return nil
			})
			return err
		}

	})
	Handle(err)
	return BlockDeserialize(block)
}

// Get the MerkleTree
func GetMerkleTreeDB(MerkleRoot []byte, db *badger.DB) *gomerkle.Tree {

	var merkleTree []byte

	err := db.View(func(txn *badger.Txn) error {
		// If errKeyNotFound  = true, Block not found
		if item, err := txn.Get(MerkleRoot); err == badger.ErrKeyNotFound {
			fmt.Printf("No Block with this ID: %d", MerkleRoot)
			return err
		} else { // get the Block

			err = item.Value(func(val []byte) error {
				merkleTree = val
				return nil
			})
			return err
		}

	})
	Handle(err)
	return DeserializeMerkleTree(merkleTree)
}

// getChain - Returns the entire chain from the Database
func GetChain(db *badger.DB) *BlockChain {
	var chain BlockChain
	var dbBlock []byte
	block_ID := GetBlockID(db)
	// itterates for the length of the Blockchain
	for i := 0; int64(i) < block_ID; i++ {

		err := db.View(func(txn *badger.Txn) error {
			// If errKeyNotFound  = true, Block not found
			if item, err := txn.Get(SerializeID(int64(i))); err == badger.ErrKeyNotFound {
				//fmt.Printf("No Block with this ID: %d", i)
				return err
			} else { // get the Block
				err = item.Value(func(val []byte) error {
					dbBlock = val
					return nil
				})

				//fmt.Println("Chian Added")
				return err
			}

		})
		Handle(err)
		chain.Blocks = append(chain.Blocks, BlockDeserialize(dbBlock))

	}

	return &chain
}

// BlockSerialize - Serialize a Block for the Database
func BlockSerialize(b *Block) []byte {
	var res bytes.Buffer
	encoder := gob.NewEncoder(&res)

	err := encoder.Encode(b)

	Handle(err)

	return res.Bytes()
}

// BlockDeserialize - Deserialize a the Database income to a Block
func BlockDeserialize(data []byte) *Block {
	var block Block

	decoder := gob.NewDecoder(bytes.NewReader(data))

	err := decoder.Decode(&block)

	Handle(err)

	return &block
}

// SerializeID - Serialize the BlockID for Database access
func SerializeID(id int64) []byte {
	blockId := make([]byte, 8)
	binary.LittleEndian.PutUint64(blockId, uint64(id))
	return blockId
}

// DeserializeID - Desirialize the Database income to a int 64
func DeserializeID(id []byte) int64 {
	return int64(binary.LittleEndian.Uint64(id))
}

// SerializeMerkleTree - Serialize a Block for the Database
func SerializeMerkleTree(t *gomerkle.Tree) []byte {

	sdata, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}
	return sdata
}

// BlockDeserialize - Deserialize a the Database income to a Block
func DeserializeMerkleTree(data []byte) *gomerkle.Tree {
	var merkletree gomerkle.Tree
	json.Unmarshal(data, &merkletree)
	return &merkletree
}

// Handle - Handles the errors
func Handle(err error) {
	if err != nil {
		log.Println(err)
	}
}
