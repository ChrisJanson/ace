package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"log"
	"time"

	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/trie"
	"github.com/onrik/gomerkle"
)

type BlockChain struct {
	Blocks   []*Block
	Db       *badger.DB
	DbMPT    *trie.Database
	Block_id int64
	MPT      *trie.Trie
}

type Block struct {
	Hash         []byte
	Data         []byte
	PrevHash     []byte
	BlockID      int64
	Timestamp    int64
	MerkleRoot   []byte
	Transactions []*Transaction
}

// Create the Hash of a Block with Data, PrevHash, BlockID and Timestamp
// @todo Add Merkle Hash Tree and MPT
func (b *Block) CreateHash() {
	info := bytes.Join([][]byte{b.Data, b.PrevHash, ToHex(b.BlockID), ToHex(b.Timestamp)}, []byte{})
	hash := sha256.Sum256(info)
	b.Hash = hash[:]
}

// Create a Block segment of the Blockchain
func (chain *BlockChain) CreateBlock(data string, prevHash []byte, tx []*Transaction) *Block {
	merkleTree := CreateMerkleTree(tx)
	merkleRoot := merkleTree.Root()
	Block := &Block{[]byte{}, []byte(data), prevHash, chain.Block_id, time.Now().Unix(), merkleRoot, tx}
	chain.Block_id++
	Block.CreateHash()
	AddMerkleTreeDB(merkleTree, chain.Db)
	return Block
}

// Create a Genesisblock segment of the Blockchain with fixed timestamp
func CreateGenesisBlock(data string, prevHash []byte, t int64) *Block {
	Block := &Block{[]byte{}, []byte(data), prevHash, 0, t, nil, nil}
	Block.CreateHash()
	return Block
}

// Add a Block to the Blockchain
func (chain *BlockChain) AddBlock(data string, tx []*Transaction) *Block {
	prevBlock := chain.Blocks[len(chain.Blocks)-1]
	new := chain.CreateBlock(data, prevBlock.Hash, tx)
	AddBlockDB(new, chain.Db)
	chain.Blocks = append(chain.Blocks, new)
	return new
}

// Create the Genesis Block
func Genesis() *Block {
	return CreateGenesisBlock("Genesis", []byte{}, 1546297200)
}

// Initialize the Blockchain with a Genesis Block
func InitBlockChain() *BlockChain {
	db := InitDatabase()
	chain := GetChain(db)
	chain.Db = db
	chain.Block_id = GetBlockID(db)
	return chain
}

// Convert int64 to []byte for Hashing
func ToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num)
	if err != nil {
		log.Panic(err)
	}

	return buff.Bytes()
}

// Create Merkle Tree root
func CreateMerkleTree(txs []*Transaction) *gomerkle.Tree {
	tree := gomerkle.NewTree(sha256.New())

	for i := 0; i < len(txs); i++ {
		tree.AddData(txs[i].TxHash)
	}

	err := tree.Generate()
	if err != nil {
		log.Panic(err)
	}

	return &tree
}
