# P2P API

### Establish a Request/Response Connection to the NearestPeer that is not a bootstrap node

```
	Testpeer := bcp2p.NearestPeer(p2p)
	bcp2p.EstablishConnection(p2p, ctx, Testpeer)
```

### Create transactions

```
		fmt.Println("creating test transactions")
		a := blockchain.CoinbaseTx("a", "100")
		b := blockchain.CoinbaseTx("b", "200")
		c := blockchain.CoinbaseTx("c", "300")
```

### Serialize a list of transactions

```
		txs := []*blockchain.Transaction{a, b, c}
		var res = utils.Serialize(txs)
```

### Publish byte data to a topic

```
		for {
			fmt.Println("publishing test transactions")
			p2p.GossipService.Publish("tx", utils.Serialize(a))
			time.Sleep(1 * time.Second)
			fmt.Println("finished publishing test transactions")

			p2p.GossipService.Publish("precommit", res)
		}
```
### Subscribe to a topic and receive byte data

```
			go bcp2p.readData(s, ctx, p2p.host)
			go bcp2p.Chat(gossipRouter, topic)

			for i in range topics {
					sub[i] = subscribe("topic")
					go readData(topic)
				}

		fmt.Println(p2p)
```

# Blockchain Core

### Create a new Blockchain and create some blocks

```
		chain := blockchain.InitBlockChain()

		txs := []*blockchain.Transaction{a, b, c}

			privatekey := blockchain.CreatePrivateKey()

			c = blockchain.SignTx(a, privatekey)
			fmt.Println(blockchain.VerifyTx(privatekey.PublicKey, c))

			fmt.Println("Private Key :")
			fmt.Printf("%x \n", privatekey)

			chain.AddBlock("First Block after Genesis", txs)
			chain.AddBlock("Second Block after Genesis", txs)
			chain.AddBlock("Third Block after Genesis", txs)
```