
# TODO

## From paper notes, to be sorted
- [ ] Code comments!
- [ ] 3 Bootstrap Nodes
- [ ] Log to file
- [ ] Send blocks from Laptops, other nodes must append
- [ ] Publish some TX
- [ ] Documentation of current state
- [ ] Startup Skript?
- [ ] How to verify sent blocks?

- [ ] Consensus
- [ ] Node validation via certificate
- [ ] Benchmarks, TX Simulator
- [ ] Client Node to provide Transactions

## Conceptual
- [ ] Timeouts (Wait4TX, Wait4ConsensLeader)
- [ ] Node Outages - how to handle?

## General
- [ ] Node: Log to file instead of STDOUT
- [ ] Node: Conceptualise node instead of multiple pieces (p2p, ledger, etc.)
- [ ] Node concept: A node consists of
   - [ ] keypair and certificate
   - [ ] TX-Buffer
   - [ ] Chain (Headers)
   - [ ] Keystore of all participants
   - [ ] Number of healthy network nodes
   - [ ] current consensus leader

## Consensus

Consensus is not yet implemented. We intend to use the PBFT Consensus Algorithm

- [ ] Initial Leader Discovery
- [ ] Determine current Leader
- [ ] Determine Time to write Block (Time-Based and TX-Buffer based)

