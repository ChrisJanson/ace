# Neuland-Chain Architectural Overview

Images created with <https://www.draw.io/>. Upload xml in diagrams folder to draw.io to edit them.

# P2P Network

![P2P-NW](doc/project documentation/diagrams/Blockchain-P2P.png)

# Blockchain Structure

![BC-Struct](doc/project documentation/diagrams/Block_structure.png)

![TX-Struct](doc/project documentation/diagrams/Transaction.png)