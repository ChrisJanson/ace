# Messages - conceptual ideas
always send the certificate in messages
always validate the certificate before initiating actions

## Requesting Blocks
### Block with Transaction
Request Transactions of a Block in Blockhash
Input: Block-Hash, TX=yes

### Request the complete Blockchain
Input: null or last known block
current incoming blocks need to be buffered!

### Request one Block
Input: Block-Hash, TX=no

## Consensus

### Broadcast TX
Sent by Leader
Input: new TX

### List of Transactions for Block-Creation
Input: Leader-TX-Pipeline, Timestamp

### ACK Liste
Input: None
Only send to Leader

### New Block (Commit)
Input: Block-Header
- To Nodes: Block-Hash
- To Client: Block-Hash + Transaction-ID

### Get current Leader
Input: None