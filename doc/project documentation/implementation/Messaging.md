# Message implementation

We support two types of messaging between Nodes:

1. Direct Communication with the nearest Neighbor
2. Publish-Subscribe (pubsub) Messaging via Topics

Messages that need to be sent to/received from all nodes or a group of nodes use the pubsub messaging schema (e.g. TX-Broadcast of new Transactions)

Messages that need to be received only once (e.g. a node requesting a block) use the direct communication schema

---

## PubSub Topics
...

## Direct Messages
...
