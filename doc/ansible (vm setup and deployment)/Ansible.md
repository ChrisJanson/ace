# Ansible How-To 

This project uses Ansible for automatic installation of project prerequisites, as well as cloning and updating the projects repository on all our vm nodes instead of doing this by hand on ~30 vm nodes. Ansible is a very lightweight solution to do this, and it needs only ssh access to the target machines and python preinstalled.

You install an Ansible host on your local development workstation and then you can launch so called playbooks on a list of hosts to perform automatic tasks.
Note: Ansible does not work on a windows based workstation. Probably in Linux Subsystem, but this has neither been tested nor verified.

Playbooks for automatic tasks can be found in the folder `playbooks`. The inventory file with the list of our vm-hosts can be found in the folder `inventory`. The ssh-keypair necessary for ansible communication can be found in the folder `ssh-keypair`.

## Quick Commands
```
Ping all hosts: 
ansible -i bchainHosts inventory/bchain -m ping

Ping one host: 
ansible -i bchainHosts uvm-isa-tokenchain-1 -m ping

Install golang playbook (needed to install golang)
ansible-galaxy install fubarhouse.golang

Install Golang on Hosts: 
ansible-playbook -i bchainHosts playbooks/deploy_golang.yml

Clone git repositories to all VMs:
ansible-playbook -i bchainHosts playbooks/clone_repositories.yml
```

## Install Ansible
<https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-the-control-machine>

## The SSH-Key
I copied the ssh-keypair from this repository `ansible/ssh-keypair/id_vm_bchain(.pub)` to `~/.ssh/id_vm_bchain`. You will need to copy that as well

## Hosts
Ansible Hosts for the vms are defined in the file `bchainHosts`. This is called an inventory file. When performing commands, the path to this Inventory file must be provided with the `-i` parameter like this: `ansible -i /path/to/bchainHosts inventory/bchain -m ping`

## Playbooks
ansible executes actions via playbooks. All playbooks are stored in YAML format, so everything in this folder ending on `*.yml` is a playbook. A playbook is executed via `ansible-playbook -i bchainHosts <playbook-name>.yml`


# Setting up new VMs from scratch

1. Edit the `bchainHosts` file. Comment the current lines below the `[bchain]` group and add the hostnames of the machines you want to set up. Prerequisite: The hosts need to have `id_vm_bchain` in their authorized keys for ansible to work.
``` 
[bchain]
# Comment this out, as we do not want to change the existing machines
# uvm-isa-tokenchain-[1:2] ansible_ssh_private_key_file=~/.ssh/id_vm_bchain.pem ansible_user=root
# uvm-isa-tokenchain-[6:23] ansible_ssh_private_key_file=~/.ssh/id_vm_bchain.pem ansible_user=root

# Add new hosts for setup. The first entry defines a range of hosts,
# the second is an example for a single host
uvm-isa-tokenchain-[24:27] ansible_ssh_private_key_file=~/.ssh/id_vm_bchain.pem ansible_user=root
uvm-isa-tokenchain-28 ansible_ssh_private_key_file=~/.ssh/id_vm_bchain.pem ansible_user=root
``` 

2. Ping the hosts your defined in `[bchain]` to check if they are reachable.
```
ansible -i bchainHosts inventory/bchain -m ping

uvm-isa-tokenchain-6 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
uvm-isa-tokenchain-1 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
[...]
```

3. Deploy go
This will take a while and take many steps. You need to install the golang-role from ansible-galaxy for this one if you haven't already done so beforehand (`ansible-galaxy install fubarhouse.golang`)
```
ansible-playbook -i bchainHosts playbooks/deploy_golang.yml
```

4. Set the go environment variables
This operation is not idempotent and will append the variables to roots bashrc every time the playbook is executed. So if this fails on some hosts, be sure to only rerun it on the failed hosts. In case of failure ansible usually creates a `.retry` file containing the failed hosts that can be handed to the ansible-playbook command.
```
ansible-playbook -i bchainHosts playbooks/set_go_env.yml
```

5. Create the symbolic link for the go executable
For reasons unknown, the role does not do this per default.
```
ansible-playbook -i bchainHosts playbooks/create_golink.yml
```

# Deploying the blockchain code to a VM for the first time

1. Prepare the ssh-environment for cloning from gitlab with the `id_vm_bchain` keypair. This playbook assumes that this keypair is to be found in `~/.ssh/id_vm_bchain` and `/.ssh/id_vm_bchain.pub`, so you need to copy those files from this repository
```
ansible-playbook -i bchainHosts playbooks/prepare_ssh.yml
```

2. Clone the ace repository
```
ansible-playbook -i bchainHosts playbooks/clone_repositories.yml
```

3. Run a go build in the repository to make the nodes fetch the dependencies
```
ansible-playbook -i bchainHosts playbooks/go_build.yml
```

4. At this point you should have a working go installation with the repository cloned. Now add the commented nodes from `bchainHosts` back to work with the full cluster

# Daily business tasks

## Update the Repository
```
ansible-playbook -i bchainHosts playbooks/update_repo.yml
```

Currently, this updates the `messaging` branch as the libp2p team is performing network tests. The playbook performs a forced update, any changes in the local repositories on the machines will be discarded and the latest changes from gitlab incorporated. So better no editing of code on the VMs! If you want to update another branch, e.g. `master`, edit the file `playbooks/update_repo.yml` and change the key `version` to master:
```
        version: messaging

        #change to
        version: master
```

## Running go run main.go
```
ansible-playbook -i bchainHosts playbooks/go_run.yml
```

This starts `go run main.go` in the repository directory of all nodes as an asynchronous task with a timeout of 180 seconds. After the timeout Ansible kills the connection. This needs to be run async, otherwise the task would only start on the first node and then block. Timeout can be extended by editing the value of `async: 180` in the `playbooks/go_run.yml` file

Although this works, you won't be able to see what is happening as currently all logging occurs to STDOUT. So in the meantime it might be more sensible to start up multiple terminals for executing the code on multiple nodes and watching what happens

