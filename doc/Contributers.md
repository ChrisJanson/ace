# Project Contributers

- Christopher Janson (Blockchain Core)
- Anatolij Mikheyev (Blockchain Core)
- Eugen Zimbelmann (P2P Networking)
- Sandra Schuhmacher  (P2P Networking)

# Communication

- Telegram group `PSE` - we will invite you 
- Skype

# Contacts

|  Name  |   Telegram   |     Skype      |             studmail              |
| :----: | :----------: | :------------: | :-------------------------------: |
| Sandra | `@yulivee07` | `sandra_17167` | `sandra.schuhmacher@stud.h-da.de` |
|        |              |                |                                   |  |
|        |              |                |                                   |  |
|        |              |                |                                   |  |