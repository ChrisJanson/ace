# Installing the projects prequisites
## Installing go

### Linux 
```
wget https://dl.google.com/go/go1.12.linux-amd64.tar.gz
rm -rf /usr/local/go/
rm /usr/bin/go

tar -C /usr/local -xzf go1.12.linux-amd64.tar.gz
ln -s /usr/local/go/bin/go /usr/bin/go

echo "export GOROOT=/usr/local/go" >> ~/.bashrc
echo "export GOPATH=/root/go" >> ~/.bashrc
echo "export GO111MODULE=on" >> ~/.bashrc

mkdir -p ~/go/src
mkdir -p ~/go/pkg
mkdir -p ~/go/bin
```

## Installing the repository

### Linux 
```
cd ~/go/src
git clone git@gitlab.com:ChrisJanson/ace.git 
cd ace
go build
```
