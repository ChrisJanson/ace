package consensus

import (
	"ace/blockchain"
	"ace/utils"
	"bufio"
	"context"

	"github.com/davecgh/go-spew/spew"
)

func (node *Node) rcvRequest(reqMsg *RequestMsg) {
	log.Info("Received Request")

	// TODO: If already processed request:
	// reply: already processed
	// -> do this by remembering the last reply message for each client -> node.LogMsg
	if node.CurrentState != nil && node.CurrentState.MsgLogs.ReqMsg != nil && node.CurrentState.MsgLogs.ReqMsg == reqMsg {
		// TODO: TEST THIS
		log.Warning("Request was already received")
	}

	if node.View.Primary != node.NodeID {
		// TODO: relay request to actual primary
		log.Warning("Received Request on non primary node, relaying the request to actual primary")
	} else {
		// Create a new state for the new consensus.
		node.createStateForNewConsensus()

		// Start the consensus process.
		prePrepareMsg, err := node.CurrentState.StartConsensus(reqMsg)
		if err != nil {
			log.Error(err)
		}

		log.Infof("Consensus Process (ViewID:%s)", node.CurrentState.ViewID)

		// broadcast preprepare
		if prePrepareMsg != nil {
			node.MessageHandler.Broadcast("preprepare", utils.Serialize(prePrepareMsg))
			log.Info("[STAGE-DONE] Pre-Prepare")
		}
	}
}

func (node *Node) rcvPrePrepare(ctx context.Context, pp *PrePrepareMsg) {

	log.Info("Received Pre-Prepare")

	// Create a new state for the new consensus.
	node.createStateForNewConsensus()

	prepareMsg := node.CurrentState.PrePrepare(pp)

	if prepareMsg != nil {
		// Attach node ID to the message
		prepareMsg.NodeID = node.NodeID

		log.Info("[STAGE-DONE] Pre-Prepare")
		node.MessageHandler.Broadcast("prepare", utils.Serialize(prepareMsg))
		log.Info("[STAGE-BEGIN] Prepare")
	}
}

func (node *Node) rcvPrepare(ctx context.Context, prepareMsg *PrepareMsg) {
	log.Info("Received Prepare")

	commitMsg := node.CurrentState.Prepare(prepareMsg)

	if commitMsg != nil {
		// Attach node ID to the message
		commitMsg.NodeID = node.NodeID

		log.Info("[STAGE-DONE] Prepare")
		node.MessageHandler.Broadcast("commit", utils.Serialize(commitMsg))
		log.Info("[STAGE-BEGIN] Commit")
	}
}

func (node *Node) rcvCommit(ctx context.Context, commitMsg *CommitMsg) {
	log.Info("Received Commit")

	replyMsg, committedMsg := node.CurrentState.Commit(commitMsg)

	if replyMsg != nil {
		if committedMsg == nil {
			log.Error("committed message is nil, even though the reply message is not nil")
		}

		// Attach node ID to the message
		replyMsg.NodeID = node.NodeID

		// Save the last version of committed messages to node.
		node.CommittedMsgs = append(node.CommittedMsgs, committedMsg)

		log.Info("[STAGE-DONE] Commit")
		node.Reply(ctx, replyMsg)
		log.Info("[STAGE-DONE] Reply")
	}
}

func (node *Node) rcvReply(msg *ReplyMsg) {
	log.Infof("Result: %s", msg.Result)
}

/***************************************************************************************************************
	TESTING
***************************************************************************************************************/
func (node *Node) handleBlockBroadcast(block blockchain.Block) {
	log.Info("Received Block Broadcast")
	spew.Dump(block)
}

func (node *Node) handleBlockMsg(rw *bufio.ReadWriter, payload []byte) {
	log.Info("RECEIVED BLOCK MESSAGE")
	var block blockchain.Block
	utils.Deserialize(payload, &block)
	spew.Dump(block)

	node.MessageHandler.Broadcast("blockbroadcast", payload)
}
