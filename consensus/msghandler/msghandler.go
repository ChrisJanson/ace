package msghandler

import (
	network "ace/consensus/msghandler/network"
	"bufio"
	"context"
	"encoding/binary"

	logging "github.com/ipfs/go-log"
	libp2pnetwork "github.com/libp2p/go-libp2p-core/network"
	peer "github.com/libp2p/go-libp2p-core/peer"
	protocol "github.com/libp2p/go-libp2p-core/protocol"
	pubsub "github.com/libp2p/go-libp2p-pubsub"
)

var log = logging.Logger("msghandler")

type MessageHandler struct {
	gossipService   *pubsub.PubSub
	topics          map[string]*pubsub.Subscription
	Network         *network.Network
	HandleDirectMsg func(*bufio.ReadWriter, []byte)
	HandleTopic     func(context.Context, string, []byte)
}

func CreateMessageHandler(ctx context.Context, HandleDirectMsg func(*bufio.ReadWriter, []byte), fHandleTopic func(context.Context, string, []byte)) *MessageHandler {
	mh := new(MessageHandler)

	mh.HandleDirectMsg = HandleDirectMsg
	mh.HandleTopic = fHandleTopic

	// CREATE THE HOST
	mh.Network = new(network.Network)
	mh.Network.CreateHost(ctx)

	// GOSSIPSUB
	var err error
	mh.gossipService, err = pubsub.NewGossipSub(ctx, mh.Network.Host)
	for mh.gossipService == nil {
	}
	if err != nil {
		log.Error(err)
	}

	mh.SubscribeToTopics(ctx, mh.Network.Config.Topics)

	// HANDLING THE DIRECT STREAMS
	mh.Network.Host.SetStreamHandler(protocol.ID(mh.Network.Config.DirectProtocol), mh.HandleDirectStream)

	// START REST OF SERVICES
	mh.Network.StartServices(ctx)

	return mh
}

func (mh *MessageHandler) SubscribeToTopics(ctx context.Context, topics []string) {
	mh.topics = make(map[string]*pubsub.Subscription)

	for _, topic := range topics {
		log.Infof("Subscribing to Topic: %s", topic)
		subscription, err := mh.gossipService.Subscribe(topic)
		if err != nil {
			log.Error(err)
		} else {
			mh.topics[topic] = subscription
			log.Info("Starting read data routine")
			go mh.ReadBroadcast(ctx, subscription)
		}
	}
}

func (mh *MessageHandler) ReadBroadcast(ctx context.Context, s *pubsub.Subscription) {
	for {
		pbMsg, err := s.Next(ctx)
		if err != nil {
			log.Error(err)
		}

		if string(pbMsg.From) != string(mh.Network.Host.ID()) {
			mh.HandleTopic(ctx, s.Topic(), pbMsg.Data)
		}
	}
}

func (mh *MessageHandler) HandleDirectStream(stream libp2pnetwork.Stream) {
	log.Info("Connection received")
	rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))
	go mh.ReadDirect(rw, stream)
}

func (mh *MessageHandler) ReadDirect(rw *bufio.ReadWriter, stream libp2pnetwork.Stream) {
	for {
		msgLength := make([]byte, 4)
		len, err := rw.Read(msgLength)
		if len == 0 {
			continue
		}
		data := binary.BigEndian.Uint32(msgLength)
		var p = make([]byte, data)
		len, err = rw.Read(p)
		if err != nil {
			stream.Close()
			break
		}

		if len == 0 {
			continue
		}

		mh.HandleDirectMsg(rw, p)
	}
}

func (mh *MessageHandler) Broadcast(topic string, msg []byte) {
	log.Info("BROADCASTING: " + topic)
	err := mh.gossipService.Publish(topic, msg)

	if err != nil {
		log.Error(err)
	}
}

func (mh *MessageHandler) WriteMessage(ctx context.Context, peerID peer.ID, bs []byte, msgSerialized []byte) {
	// ESTABLISH CONNECTION
	rw, stream := mh.Network.EstablishConnection(ctx, peerID)
	go mh.ReadDirect(rw, stream)

	_, err := rw.Write(append(bs, msgSerialized...))

	if err != nil {
		log.Error(err)
	}

	err = rw.Flush()
	if err != nil {
		log.Error(err)
	}
}
