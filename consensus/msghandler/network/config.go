package network

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

type Config struct {
	RendezvousString     string   `json:"rendezvous"`
	Protocol             string   `json:"protocol"`
	Transport            string   `json:"transport"`
	ListenPort           int      `json:"listenport"`
	BootstrapPeers       []string `json:"bootstrappeers"`
	PrimaryIP            string   `json:"primary"`
	ListenAddresses      []string
	ProtocolID           string   `json:"protocolid"`
	Topics               []string `json:"topics"`
	DirectProtocol       string   `json:"directprotocol"`
	Bootstrapserviceport int      `json:"bootstrapserviceport"`
}

func ParseConfig() (Config, error) {
	config := Config{}

	configFile, err := os.Open("consensus/msghandler/network/config.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)

	myAddr := "/" + config.Protocol + "/" + GetLocalIP() + "/" + config.Transport + "/" + strconv.Itoa(config.ListenPort)
	//ma, err := maddr.NewMultiaddr(myAddr)
	if err != nil {
		panic(err)
	}
	config.ListenAddresses = []string{myAddr}

	return config, nil
}
