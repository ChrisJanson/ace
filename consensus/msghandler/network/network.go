package network

import (
	"bufio"
	"context"
	inet "net"
	"strconv"
	"strings"
	"sync"
	"time"

	logging "github.com/ipfs/go-log"
	libp2p "github.com/libp2p/go-libp2p"
	host "github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	peer "github.com/libp2p/go-libp2p-core/peer"
	protocol "github.com/libp2p/go-libp2p-core/protocol"
	disc "github.com/libp2p/go-libp2p-discovery"
	kaddht "github.com/libp2p/go-libp2p-kad-dht"
	kb "github.com/libp2p/go-libp2p-kbucket"
	peerstore "github.com/libp2p/go-libp2p-peerstore"
	routing "github.com/libp2p/go-libp2p-routing"
	secio "github.com/libp2p/go-libp2p-secio"
	tcp "github.com/libp2p/go-tcp-transport"
	ws "github.com/libp2p/go-ws-transport"
	ma "github.com/multiformats/go-multiaddr"
)

var log = logging.Logger("network")

type Network struct {
	rtDis            *disc.RoutingDiscovery
	DHT              *kaddht.IpfsDHT
	Host             host.Host
	Config           Config
	Peers            map[string]peerstore.PeerInfo
	RoutingTable     *kb.RoutingTable
	IAmBootstrapNode bool
}

func (nw *Network) CreateHost(ctx context.Context) {

	// PEER MAP
	nw.Peers = make(map[string]peerstore.PeerInfo)

	// PARSING CONFIG
	var err error
	nw.Config, err = ParseConfig()
	if err != nil {
		panic(err)
	}

	// AM I BOOSTRAPNODE?
	nw.IAmBootstrapNode = AmIBootstrapNode(nw.Config.BootstrapPeers)

	// TRANSPORT
	transports := libp2p.ChainOptions(
		libp2p.Transport(tcp.NewTCPTransport),
		libp2p.Transport(ws.New),
	)

	// SECURITY
	security := libp2p.Security(secio.ID, secio.New)

	// LISTENING ADDRESSES
	listenAddrs := libp2p.ListenAddrStrings(nw.Config.ListenAddresses[0])

	// ROUTING
	newDHT := func(h host.Host) (routing.PeerRouting, error) {
		var err error
		nw.DHT, err = kaddht.New(ctx, h)
		if err != nil {
			log.Error(err)
		}
		return nw.DHT, err
	}
	routing := libp2p.Routing(newDHT)

	// CREATING THE HOST
	nw.Host, err = libp2p.New(ctx, transports, listenAddrs, security, routing)
	if err != nil {
		panic(err)
	}

	log.Infof("Host created. We are: %s", nw.Host.ID().Pretty())
}

func (nw *Network) StartServices(ctx context.Context) {
	// STARTING BOOTSTRAPSERVICE
	if nw.IAmBootstrapNode {
		StartBootstrapService(nw.Host.ID().Pretty(), nw.Config.Bootstrapserviceport)
	}

	// Connecting to bootstrap nodes
	// They will tell us about the other nodes in the network.
	if !nw.IAmBootstrapNode {

		var wg sync.WaitGroup

		multiaddress := nw.GetBootstrapnodeMuliAddr()

		peerinfo, _ := peerstore.InfoFromP2pAddr(multiaddress)
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := nw.Host.Connect(ctx, *peerinfo); err != nil {
				log.Errorf("Failed to connect to Bootstrapnode: %s", err)
			} else {
				log.Infof("Connection established with bootstrap node: %s", *peerinfo)
			}
		}()

		wg.Wait()

		// Wait for the Heartbeat of GossipSub
		time.Sleep(2 * time.Second)
	}

	nw.RoutingTable = nw.DHT.RoutingTable()

	// Bootstrap the DHT. In the default configuration, this spawns a Background
	// thread that will refresh the peer table every five minutes.
	log.Info("Bootstrapping the DHT")
	var bootstrapconfig = kaddht.BootstrapConfig{
		Queries: 1,
		Period:  time.Duration(1 * time.Minute),
		Timeout: time.Duration(5 * time.Second),
	}
	var err error
	if err = nw.DHT.BootstrapWithConfig(ctx, bootstrapconfig); err != nil {
		log.Error("Bootstrapping the DHT failed")
		panic(err)
	}

	// CREATING ROUTINGDISCOVERY
	routingDiscovery := disc.NewRoutingDiscovery(nw.DHT)
	nw.rtDis = routingDiscovery

	if !nw.IAmBootstrapNode {
		// Use a rendezvous point to announce ourselves
		log.Info("Announcing ourselves...")
		disc.Advertise(ctx, routingDiscovery, nw.Config.RendezvousString)
		log.Info("Successfully announced!")

		// Look for others who have announced themselves
		nw.SearchActivePeers(ctx)
	}
}

func AmIBootstrapNode(bootstrap_ips []string) bool {
	my_ip := GetLocalIP()

	for i := 0; i < len(bootstrap_ips); i++ {
		if my_ip == bootstrap_ips[i] {
			return true
		}
	}
	return false
}

func StartBootstrapService(id string, bootstrapserviceport int) {
	log.Info("Starting Bootstrap Service...")

	// listen on all interfaces
	ln, _ := inet.Listen("tcp", ":"+strconv.Itoa(bootstrapserviceport))

	go func() {
		for {
			// accept connection on port
			conn, _ := ln.Accept()

			// send id string back to client
			conn.Write([]byte(id + "\n"))
			conn.Close()
		}
	}()
}

func (nw *Network) GetBootstrapnodeMuliAddr() ma.Multiaddr {
	// connect to bootstrapnodes
	for _, ip := range nw.Config.BootstrapPeers {
		conn, err := inet.Dial("tcp", ip+":"+strconv.Itoa(nw.Config.Bootstrapserviceport))

		if err != nil {
			log.Errorf("Error connecting to Bootstrapnode: %s", err)
			continue
		}

		// listen for reply
		id, _ := bufio.NewReader(conn).ReadString('\n')
		id = id[:len(id)-1]
		log.Infof("Got ID from bootstrap service: %s", id)

		fullAddr := "/" + nw.Config.Protocol + "/" + ip + "/" + nw.Config.Transport + "/" + strconv.Itoa(nw.Config.ListenPort) + "/" + "ipfs" + "/" + id
		ma, err := ma.NewMultiaddr(fullAddr)

		if err != nil {
			log.Errorf("Error building Multiaddress: %s", err)
			continue
		}

		return ma
	}
	return nil
}

func (nw *Network) SearchActivePeers(ctx context.Context) {
	peerChan, err := disc.FindPeers(ctx, nw.rtDis, nw.Config.RendezvousString)
	if err != nil {
		panic(err)
	}

	for _, peer := range peerChan {
		if peer.ID == nw.Host.ID() || len(peer.Addrs) <= 0 {
			continue
		}

		pID := peer.ID.Pretty()
		if pID == "" {
			continue
		}

		nw.Host.Peerstore().AddAddr(peer.ID, peer.Addrs[0], peerstore.PermanentAddrTTL)
		nw.Peers[pID] = peer
	}
}

// EstablishConnection creates a stream to a given peer
func (nw *Network) EstablishConnection(ctx context.Context, peerID peer.ID) (*bufio.ReadWriter, network.Stream) {
	log.Infof("Creating new Stream to: ", peerID)
	stream, err := nw.Host.NewStream(ctx, peerID, protocol.ID(nw.Config.DirectProtocol))

	if err != nil {
		log.Errorf("Connection failed: %s", err)
		return nil, nil
	} else {
		log.Infof("Connected to peer: %s", peerID.String())
		rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))
		return rw, stream
	}
}

func (nw *Network) GetNearestPeerID() peer.ID {
	var nextPeers = nw.RoutingTable.NearestPeers(kb.ConvertPeerID(nw.Host.ID()), 1)

	return nextPeers[0]
}

func (nw *Network) GetPeers() peer.IDSlice {
	peers := nw.Host.Peerstore().Peers()
	var filtered peer.IDSlice
	for _, peer := range peers {
		if peer.Pretty() != nw.Host.ID().Pretty() {
			filtered = append(filtered, peer)
		}
	}

	return filtered
}

func GetLocalIP() string {
	conn, err := inet.Dial("udp", "8.8.8.8:8080")
	if err != nil {
		log.Error(err)
		panic(err)
	}

	ip := strings.Split(conn.LocalAddr().String(), ":")[0]
	conn.Close()
	return ip
}
