package consensus

import peer "github.com/libp2p/go-libp2p-core/peer"

type Message struct {
	MsgType MessageType
	Payload []byte
}

type MessageType int32

const (
	REQUEST MessageType = iota + 1
	REPLY
	/* TESTING: */
	TRANSACTION
	TRANSACTIONS
	BLOCK
	BLOCK_REQ
)

type RequestMsg struct {
	Timestamp  int64
	ClientID   peer.ID
	Operation  string
	SequenceID int64
}

type ReplyMsg struct {
	ViewID    int64
	Timestamp int64
	ClientID  peer.ID
	NodeID    peer.ID
	Result    string
}

type PrePrepareMsg struct {
	ViewID     int64
	SequenceID int64
	Digest     string
	RequestMsg *RequestMsg
}

type PrepareMsg struct {
	ViewID     int64
	SequenceID int64
	Digest     string
	NodeID     peer.ID
}

type CommitMsg struct {
	ViewID     int64
	SequenceID int64
	Digest     string
	NodeID     peer.ID
}
