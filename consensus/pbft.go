package consensus

import (
	"ace/blockchain"
	"ace/consensus/msghandler"
	"ace/consensus/msghandler/network"
	"ace/utils"
	"bufio"
	"context"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"time"

	logging "github.com/ipfs/go-log"
	peer "github.com/libp2p/go-libp2p-core/peer"
)

var log = logging.Logger("consensus")

type Stage int

const (
	Idle        Stage = iota // Node is created successfully, but the consensus process is not started yet.
	PrePrepared              // The ReqMsgs is processed successfully. The node is ready to head to the Prepare stage.
	Prepared                 // Same with `prepared` stage explained in the original paper.
	Committed                // Same with `committed-local` stage explained in the original paper.
)

type State struct {
	ViewID         int64
	MsgLogs        *MsgLogs
	LastSequenceID int64
	CurrentStage   Stage
}

type MsgLogs struct {
	ReqMsg      *RequestMsg
	PrepareMsgs map[string]*PrepareMsg
	CommitMsgs  map[string]*CommitMsg
}

type MsgBuffer struct {
	ReqMsgs        []*RequestMsg
	PrePrepareMsgs []*PrePrepareMsg
	PrepareMsgs    []*PrepareMsg
	CommitMsgs     []*CommitMsg
}

type View struct {
	ID      int64
	Primary peer.ID
}

type Node struct {
	NodeID         peer.ID
	View           *View
	CurrentState   *State
	CommittedMsgs  []*RequestMsg // kinda block.
	MsgBuffer      *MsgBuffer
	MessageHandler *msghandler.MessageHandler
}

// f: Number of max. possible faulty nodes:
// f = <number_of_replicas> / 3
// n = 4 and f = 1 for testing.
const f = 1

func NewNode(ctx context.Context) *Node {
	// TODO: viewID calculation?
	const viewID = 0

	node := &Node{
		View: &View{
			ID: viewID,
		},

		// Consensus-related
		CurrentState:  nil,
		CommittedMsgs: make([]*RequestMsg, 0),
		MsgBuffer: &MsgBuffer{
			ReqMsgs:        make([]*RequestMsg, 0),
			PrePrepareMsgs: make([]*PrePrepareMsg, 0),
			PrepareMsgs:    make([]*PrepareMsg, 0),
			CommitMsgs:     make([]*CommitMsg, 0),
		},
	}
	node.MessageHandler = msghandler.CreateMessageHandler(ctx, node.HandleDirectMsg, node.HandleTopic)
	node.NodeID = node.MessageHandler.Network.Host.ID()
	if node.MessageHandler.Network.Config.PrimaryIP == network.GetLocalIP() {
		node.View.Primary = node.NodeID
	} else {
		node.View.Primary = ""
	}

	return node
}

func (node *Node) HandleTopic(ctx context.Context, topic string, payload []byte) {
	switch topic {
	case "preprepare":
		var pp PrePrepareMsg
		utils.Deserialize(payload, &pp)
		node.rcvPrePrepare(ctx, &pp)
	case "prepare":
		var p PrepareMsg
		utils.Deserialize(payload, &p)
		node.rcvPrepare(ctx, &p)
	case "commit":
		var c CommitMsg
		utils.Deserialize(payload, &c)
		node.rcvCommit(ctx, &c)
	/* TESTING */
	case "blockbroadcast":
		var block blockchain.Block
		utils.Deserialize(payload, &block)
		node.handleBlockBroadcast(block)
	}
	/*
		case "tx":
			log.Info("[PUBSUB] RECEIVED TX")
			cons.handleTXMsg(payload)
		case "tx[]":
			log.Info("[PUBSUB] RECEIVED TX[]")
			cons.handleTXQueue(payload)
	*/
}

func (node *Node) HandleDirectMsg(rw *bufio.ReadWriter, data []byte) {
	var msg Message
	utils.Deserialize(data, &msg)

	switch msg.MsgType {
	case REQUEST:
		var rq RequestMsg
		utils.Deserialize(msg.Payload, &rq)
		node.rcvRequest(&rq)
	/* TESTING */
	case BLOCK:
		node.handleBlockMsg(rw, msg.Payload)
		/*
			case TRANSACTION:
				cons.handleTXMsg(msg.Payload)
			case TRANSACTIONS:
				cons.handleTXQueue(msg.Payload)
			case BLOCK:

		*/
	}
}

func (node *Node) Reply(ctx context.Context, msg *ReplyMsg) {
	// Print all committed messages.
	for _, value := range node.CommittedMsgs {
		fmt.Printf("Committed value: %s, %d, %s, %d", value.ClientID, value.Timestamp, value.Operation, value.SequenceID)
	}
	fmt.Print("\n")

	size, serializedMsg := CreateMessage(REPLY, msg)

	// TODO: Send Reply to Client
	// at the moment: sending reply to primary
	node.MessageHandler.WriteMessage(ctx, node.View.Primary, size, serializedMsg)
}

func (node *Node) createStateForNewConsensus() {
	// Check if there is an ongoing consensus process.
	if node.CurrentState != nil {
		log.Error("another consensus is ongoing")
	}

	// Get the last sequence ID
	var lastSequenceID int64
	if len(node.CommittedMsgs) == 0 {
		lastSequenceID = -1
	} else {
		lastSequenceID = node.CommittedMsgs[len(node.CommittedMsgs)-1].SequenceID
	}

	// Create a new state for this new consensus process in the Primary
	node.CurrentState = CreateState(node.View.ID, lastSequenceID)

	log.Info("Create the replica status")
}

// lastSequenceID will be -1 if there is no last sequence ID.
func CreateState(viewID int64, lastSequenceID int64) *State {
	return &State{
		ViewID: viewID,
		MsgLogs: &MsgLogs{
			ReqMsg:      nil,
			PrepareMsgs: make(map[string]*PrepareMsg),
			CommitMsgs:  make(map[string]*CommitMsg),
		},
		LastSequenceID: lastSequenceID,
		CurrentStage:   Idle,
	}
}

func (state *State) StartConsensus(request *RequestMsg) (*PrePrepareMsg, error) {
	// sequenceID will be the index of this message.
	sequenceID := time.Now().UnixNano()

	// Find the unique and largest number for the sequence ID
	if state.LastSequenceID != -1 {
		for state.LastSequenceID >= sequenceID {
			sequenceID += 1
		}
	}

	// Assign a new sequence ID to the request message object.
	request.SequenceID = sequenceID

	// Save ReqMsgs to its logs.
	state.MsgLogs.ReqMsg = request

	// Get the digest of the request message
	digest, err := digest(request)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	// Change the stage to pre-prepared.
	state.CurrentStage = PrePrepared

	return &PrePrepareMsg{
		ViewID:     state.ViewID,
		SequenceID: sequenceID,
		Digest:     digest,
		RequestMsg: request,
	}, nil
}

func (state *State) PrePrepare(prePrepareMsg *PrePrepareMsg) *PrepareMsg {
	// Get ReqMsgs and save it to its logs like the primary.
	state.MsgLogs.ReqMsg = prePrepareMsg.RequestMsg

	// Verify if v, n(a.k.a. sequenceID), d are correct.
	if !state.verifyMsg(prePrepareMsg.ViewID, prePrepareMsg.SequenceID, prePrepareMsg.Digest) {
		log.Error("pre-prepare message is corrupted")
		return nil
	}

	// Change the stage to pre-prepared.
	state.CurrentStage = PrePrepared

	return &PrepareMsg{
		ViewID:     state.ViewID,
		SequenceID: prePrepareMsg.SequenceID,
		Digest:     prePrepareMsg.Digest,
	}
}

func (state *State) Prepare(prepareMsg *PrepareMsg) *CommitMsg {
	if !state.verifyMsg(prepareMsg.ViewID, prepareMsg.SequenceID, prepareMsg.Digest) {
		log.Error("prepare message is corrupted")
		return nil
	}

	// Append msg to its logs
	state.MsgLogs.PrepareMsgs[prepareMsg.NodeID.Pretty()] = prepareMsg

	// Print current voting status
	fmt.Printf("[Prepare-Vote]: %d\n", len(state.MsgLogs.PrepareMsgs))

	if state.prepared() {
		// Change the stage to prepared.
		state.CurrentStage = Prepared

		return &CommitMsg{
			ViewID:     state.ViewID,
			SequenceID: prepareMsg.SequenceID,
			Digest:     prepareMsg.Digest,
		}
	}

	return nil
}

func (state *State) Commit(commitMsg *CommitMsg) (*ReplyMsg, *RequestMsg) {
	if !state.verifyMsg(commitMsg.ViewID, commitMsg.SequenceID, commitMsg.Digest) {
		log.Error("commit message is corrupted")
		return nil, nil
	}

	// Append msg to its logs
	state.MsgLogs.CommitMsgs[commitMsg.NodeID.Pretty()] = commitMsg

	// Print current voting status
	fmt.Printf("[Commit-Vote]: %d\n", len(state.MsgLogs.CommitMsgs))

	if state.committed() {
		// This node executes the requested operation locally and gets the result.
		result := "Executed"

		// Change the stage to prepared.
		state.CurrentStage = Committed

		return &ReplyMsg{
			ViewID:    state.ViewID,
			Timestamp: state.MsgLogs.ReqMsg.Timestamp,
			ClientID:  state.MsgLogs.ReqMsg.ClientID,
			Result:    result,
		}, state.MsgLogs.ReqMsg
	}

	return nil, nil
}

func (state *State) verifyMsg(viewID int64, sequenceID int64, digestGot string) bool {
	// Wrong view. That is, wrong configurations of peers to start the consensus.
	if state.ViewID != viewID {
		return false
	}

	// Check if the Primary sent fault sequence number. => Faulty primary.
	// TODO: adopt upper/lower bound check.
	if state.LastSequenceID != -1 {
		if state.LastSequenceID >= sequenceID {
			return false
		}
	}

	digest, err := digest(state.MsgLogs.ReqMsg)
	if err != nil {
		fmt.Println(err)
		return false
	}

	// Check digest.
	if digestGot != digest {
		return false
	}

	return true
}

func (state *State) prepared() bool {
	if state.MsgLogs.ReqMsg == nil {
		return false
	}

	if len(state.MsgLogs.PrepareMsgs) < 2*f {
		return false
	}

	return true
}

func (state *State) committed() bool {
	if !state.prepared() {
		return false
	}

	if len(state.MsgLogs.CommitMsgs) < 2*f {
		return false
	}

	return true
}

func digest(object interface{}) (string, error) {
	msg, err := json.Marshal(object)

	if err != nil {
		return "", err
	}

	return utils.Hash(msg), nil
}

func CreateMessage(mType MessageType, Payload interface{}) ([]byte, []byte) {
	msg := Message{
		MsgType: mType,
		Payload: utils.Serialize(Payload),
	}
	msgSerialized := utils.Serialize(msg)
	sizeOfMessage := len(msgSerialized)
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, uint32(sizeOfMessage))
	return bs, msgSerialized
}

// Request (1-1) to (believed) Primary <o,t,c>
// block and wait for reply
func (node *Node) SendRequest(ctx context.Context) {
	var peerID peer.ID

	if node.View.Primary == "" {
		peers := node.MessageHandler.Network.GetPeers()

		if peers.Len() == 0 {
			peerID = node.MessageHandler.Network.GetNearestPeerID()
		} else {
			peerID = peers[0]
		}

	} else {
		peerID = node.View.Primary
	}

	log.Info("Sending [REQUEST] to Peer: " + peerID.String())

	rq := RequestMsg{time.Now().Unix(), node.NodeID, "wow", 0}
	size, msgSerialized := CreateMessage(REQUEST, rq)
	node.MessageHandler.WriteMessage(ctx, peerID, size, msgSerialized)
}

func (node *Node) SendBlock(ctx context.Context, peerID peer.ID, block blockchain.Block) {
	log.Info("Sending a Block to Peer " + peerID.String())
	bs, msgSerialized := CreateMessage(BLOCK, block)
	node.MessageHandler.WriteMessage(ctx, peerID, bs, msgSerialized)
}
